<?php

declare(strict_types=1);

namespace Charm;

use Psr\Log\LoggerInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Closure;

/**
 * Router
 * ======.
 *
 * A minimal and fast router implementation. Usage:
 *
 * ```
 * $router = new Router('https://www.example.com');
 *
 * $router->add('GET', '/users/{id}', function($id) {});
 *
 * $match = $router->resolve('GET', '/users/123');
 * // Result: [ closure, [ 'id' => 123 ] ]
 *
 * $match = $router->resolve('POST', '/users/123');
 * // Result: exception Router\Error('Not supported', 501);
 *
 * $match = $router->resolve('POST', '/users/123/subroute');
 * // Result: exception Router\Error('Not found', 404);
 * ```
 *
 * Advanced patterns
 * -----------------
 *
 * The router supports more advanced patterns using regular expressions:
 *
 * $id must be a number:
 * `$router->add('GET', '/users/{id:\d+}', "Handler can be anything");`
 *
 * Path extensions are simply a part of the string we match:
 * `$router->add('GET', '/users/{id:\d+}.html', function() {});`
 *
 * Weakness
 * --------
 *
 * The biggest weakness, and the primary reason this router is quite fast
 * both for adding routes (no real need for caching), and resolving them, is
 * that it tokenizes the path on slashes /.
 *
 * If you plan to have a site with a lot of routes in a single "folder",
 * then this router might not suit you.
 *
 * Performance
 * -----------
 *
 * I haven't compared this router with famous implementations like Nikics'
 * FastRoute, but I added 10000 routes in 0.05 seconds, and resolved as many
 * in 0.05 on my server.
 *
 * If you have 10000 unique routes, you are putting too much logic inside
 * the router, and you should reconsider your approach. That's my opinion.
 */
class Router implements MiddlewareInterface
{
    const DEFAULT_CONFIG = [
        /**
         * The base URL for the router
         */
        'base_url' => '/',

        /**
         * Function which will invoke the request handler and return a ResponseInterface.
         * @var function(mixed $handler, array $vars): ResponseInterface
         */
        'invoker' => null,

        /**
         * @var Psr\Log\LoggerInterface
         */
        'logger' => null,
    ];
    protected $config;

    // child nodes that match without a regex, identified by their path component
    protected $fixedChildren = [];

    // child nodes that match with a regex, identified by their reduced regex path component
    protected $patternChildren = [];

    protected $root;
    protected $parent;
    protected $handlers = [];
    protected $reverse;

    public function __construct(array $config=[])
    {
        $this->config = $config + self::DEFAULT_CONFIG;
        $this->root = $this; // Will be overwritten when this is a child router instance
    }

    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface {
        $path = trim(explode("?", $request->getRequestTarget())[0], "/");
        $method = $request->getMethod();
        try {
            $resolved = $this->resolve($method, $path);

            list ($controller, $vars) = $resolved;

            if (!empty($this->config['invoker'])) {
                $invoker = $this->config['invoker'];
                return $invoker($controller, $vars);
            }

            if ($controller instanceof RequestHandlerInterface) {
                return $controller->handle($request);
            } elseif (is_callable($controller)) {
                $result = $controller($request);
                if ($result instanceof ResponseInterface) {
                    return $result;
                }
                throw new Router\Error("Handler for '$path' did not return a ResponseInterface object", 500);
            } else {
                throw new Router\Error("Handler for '$path' is not a RequestHandlerInterface or a callable", 500);
            }
            return $handler->handle($request);
        } catch (Router\Error $error) {
            if ($error->getCode() >= 500) {
                throw $error;
            }
            return $handler->handle($request);
        }
    }

    public function url(string $method, $handler, ...$args): ?string
    {
        if ($hash = $this->getHash($handler)) {
            $method = strtoupper($method);
            $matches = $this->root->reverse[$hash] ?? null;
            if (null === $matches) {
                if (\is_object($handler)) {
                    throw new Router\Error('No routes have been registered for this handler', 500);
                }
                if (\is_callable($handler)) {
                    throw new Router\Error('No routes have been registered for the '.json_encode($handler).' callable', 500);
                }
                throw new Router\Error('No routes have been registered for the '.json_encode($handler).' non-callable handler', 500);
            }
            foreach ($matches as $match) {
                if ($match[0] === $method) {
                    return $match[2]->buildPath($args);
                }
            }

            throw new Router\Error("The verb '$method' is not supported", 501);
        } else {
            throw new Router\Error("The handler '$method' is not hashable", 500, null, [500,  'Internal Server Error']);
        }
    }

    /**
     * Resolve a path in the router.
     *
     * @param string $method The HTTP method we want to resolve for
     * @param string $path   The path to resolve for
     *
     * @return array array with the handler and all variables that were matched
     */
    public function resolve(string $method, string $path)
    {
        $path = ltrim($path, '/');
        $root = $this;
        $vars = [];
        if ('' !== $path) {
            foreach (explode('/', $path) as $component) {
                if (\array_key_exists($component, $root->fixedChildren)) {
                    $root = $root->fixedChildren[$component];
                    continue;
                } else {
                    foreach ($root->patternChildren as $regex => $child) {
                        $hits = preg_match('#'.$regex.'$#A', $component, $matches);
                        if ($hits > 0) {
                            for ($i = 1; \array_key_exists($i, $matches); ++$i) {
                                $vars[] = $matches[$i];
                            }
                            $root = $child;
                            continue 2;
                        }
                    }
                }
                throw new Router\Error("Path '$path' not found", 404);
            }
        }
        $method = strtoupper($method);
        if (!\array_key_exists($method, $root->handlers)) {
            throw new Router\Error("Method '$method' not implemented", 405);
        }

        $handler = $root->handlers[$method];

        if (\count($handler[0]) !== \count($vars)) {
            // Testing here, just as a precaution for now. This shouldn't be needed.
            throw new Router\Error("Wrong number of variables when resolving", 500);
        }

        return [$handler[1], array_combine($handler[0], $vars)];
    }

    /**
     * Add a routing path to the router instance.
     *
     * @param string $method  The HTTP method(s) we wish to handle, separated by |
     * @param string $path    The path (relative to base_url, with {varName} as path components
     * @param mixed  $handler a value that will be returned from this route, normally a callable
     */
    public function add(string $method, string $path, $handler): self
    {
        $path = ltrim($path, '/');
        $root = $this;

        // A list of all variables that the handler expects
        $vars = [];
        $translate = [];

        if ('' !== $path) {
            foreach (explode('/', $path) as $component) {
                if (($start = strpos($component, '{')) !== false) {
                    // Assume this component is dynamic
                    $regexReplace = [];
                    $varReplace = [];
                    $template = preg_replace_callback(
                        '/{(?<identifier>[a-zA-Z_\\x80-\\xff][a-zA-Z0-9_\\x80-\\xff]*)(:(?<pattern>.*))?}/',
                        function (array $matches) use (&$translate, &$vars, $method, $path) {
                            $pattern = '.+';
                            extract($matches); // will overwrite $pattern, nice!

                            if (\in_array($identifier, $vars)) {
                                throw new Router\Error("Can't use the identifier '$identifier' twice in the same route ($method $path)");
                            }

                            // For translating the resulting string
                            $translate[$key = '//'.\count($vars).'//'] = '('.$pattern.')';

                            // Used to have a list of variables that a path needs - when building a URL from a closure
                            $vars[] = $identifier;

                            return $key;

                            return $key;
                        },
                        $component
                    );

                    if (\count($vars) > 0) {
                        // Our assumption was correct, the path component is dynamic
                        $regexComponent = strtr(preg_quote($template), $translate);
                        $baseUrl = str_replace(array_keys($translate), '###', $template);

                        // No part of the path can contain forward slash, so we use it as a prefix
                        if (!isset($root->patternChildren[$regexComponent])) {
                            $root->patternChildren[$regexComponent] = new static([ 'base_url' => $baseUrl ] + $this->config);
                            $root->patternChildren[$regexComponent]->root = $this->root;
                            $root->patternChildren[$regexComponent]->parent = $root;
                        }
                        $root = $root->patternChildren[$regexComponent];
                        $rebuildPath[] = '###';
                        continue;
                    }
                }
                $rebuildPath[] = $component;

                if (!isset($root->fixedChildren[$component])) {
                    $root->fixedChildren[$component] = new static([ 'base_url' => $component ] + $this->config);
                    $root->fixedChildren[$component]->root = $this->root;
                    $root->fixedChildren[$component]->parent = $root;
                }
                $root = $root->fixedChildren[$component];
            }
        }

        foreach (explode('|', $method) as $method) {
            $root->handle($method, $vars, $handler);
        }

        return $root;
    }

    /**
     * Add a request handler on any endpoint. For example:
     * `$router->add('/')->handle('POST', function() {});`.
     *
     * @param string   $method  The HTTP verb (GET, POST, PUT etc)
     * @param callable $handler The function that will process this URL
     */
    protected function handle(string $method, array $vars, $handler)
    {
        $method = strtoupper($method);
        $this->handlers[$method] = [$vars, $handler];
        if ($hash = $this->getHash($handler)) {
            $this->root->reverse[$hash][] = [$method, $vars, $this];
        }
    }

    protected function getHash($handler)
    {
        if (!\is_object($handler) && \is_array($handler) && \is_callable($handler)) {
            return spl_object_hash(Closure::fromCallable($handler));
        } elseif (is_scalar($handler) || \is_array($handler)) {
            return md5(serialize($handler));
        } elseif (\is_object($handler)) {
            return spl_object_hash($handler);
        } else {
            return null;
        }
    }

    protected function buildPath(array $vars = []): string
    {
        $str = '';
        $leaf = $this;
        while ($leaf) {
            $component = $leaf->config['base_url'];
            if ($leaf->parent) {
                // For the top-most router, we may want to strip the rightmost slash
                $str = $component.('' === $str ? '' : '/'.$str);
            } else {
                $str = rtrim($component, '/').('' === $str ? '' : '/'.$str);
            }
            $leaf = $leaf->parent;
        }
        if ('' === $this->config['base_url']) {
            $str .= '/';
        }
        $varNum = 0;
        $str = preg_replace_callback('/###/', function ($_) use ($vars, &$varNum) {
            if (!\array_key_exists($varNum, $vars)) {
                throw new Router\Error('Need more variables to build the path', 500);
            }

            return $vars[$varNum++];
        }, $str);
        if (\count($vars) > $varNum) {
            throw new Router\Error('Too many variables for the route', 500);
        }

        return $str;
    }
}
